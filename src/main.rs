#![warn(clippy::pedantic)]
#![warn(clippy::nursery)]
#![warn(clippy::cargo)]
// snowflakes are a thing :/
#![allow(clippy::unreadable_literal)]
#![allow(clippy::multiple_crate_versions)]

use futures::StreamExt;
use std::env;
use std::error::Error;
use twilight_gateway::{Event, Intents, Shard};
use twilight_model::gateway::payload::{UpdateVoiceState, VoiceServerUpdate, VoiceStateUpdate};
use twilight_model::id::{ChannelId, GuildId};

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    let token = env::var("TOKEN")?;
    let (shard, mut events) =
        Shard::new(token, Intents::GUILD_MESSAGES | Intents::GUILD_VOICE_STATES);

    shard.start().await?;
    println!("Created shard");

    let mut state: Option<Box<VoiceStateUpdate>> = None;
    let mut server: Option<VoiceServerUpdate> = None;

    while let Some(event) = events.next().await {
        match event {
            Event::VoiceStateUpdate(evt) => {
                state.replace(evt);

                if server.is_some() {
                    let server = server.take().unwrap();
                    let state = state.take().unwrap();

                    tokio::spawn(miku::connect_to_voice_gateway(state, server));
                }
            }
            Event::VoiceServerUpdate(evt) => {
                server.replace(evt);

                if state.is_some() {
                    let state = state.take().unwrap();
                    let server = server.take().unwrap();

                    tokio::spawn(miku::connect_to_voice_gateway(state, server));
                }
            }

            Event::MessageCreate(evt) => match evt.content.as_str() {
                "join~" => {
                    shard
                        .command(&UpdateVoiceState::new(
                            GuildId(483409624100503562),
                            ChannelId(483409815134011392),
                            false,
                            false,
                        ))
                        .await?;
                }
                "leave~" => {
                    shard
                        .command(&UpdateVoiceState::new(
                            GuildId(483409624100503562),
                            None,
                            false,
                            false,
                        ))
                        .await?;
                }
                _ => {}
            },
            _ => {}
        }
    }

    Ok(())
}
