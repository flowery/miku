use serde::{Deserialize, Serialize};
use std::fmt::Debug;

pub trait HasPayload {}

#[derive(Serialize, Deserialize)]
pub struct Payload<T: Debug + HasPayload> {
    op: u8,
    d: T,
}

impl<T: HasPayload + Debug> Payload<T> {
    pub fn d(&self) -> &T {
        &self.d
    }

    pub fn full_d(self) -> T {
        self.d
    }

    pub fn new(op: u8, d: T) -> Self {
        Self { op, d }
    }
}

#[derive(Debug, Deserialize)]
pub struct Hello {
    heartbeat_interval: f64,
}

impl Hello {
    pub const fn heartbeat_interval(&self) -> f64 {
        self.heartbeat_interval
    }
}

impl HasPayload for Hello {}
impl HasPayload for u32 {}

#[derive(Debug, Serialize)]
pub struct Identify {
    server_id: String,
    user_id: String,
    session_id: String,
    token: String,
}

impl Identify {
    pub const fn new(
        server_id: String,
        user_id: String,
        session_id: String,
        token: String,
    ) -> Self {
        Self {
            server_id,
            user_id,
            session_id,
            token,
        }
    }
}

impl HasPayload for Identify {}

#[derive(Debug, Deserialize)]
pub struct Ready {
    ssrc: u32,
    ip: String,
    port: u16,
    modes: Vec<String>,
}

impl Ready {
    pub const fn ssrc(&self) -> u32 {
        self.ssrc
    }

    pub const fn ip(&self) -> &String {
        &self.ip
    }

    pub const fn port(&self) -> u16 {
        self.port
    }

    pub const fn modes(&self) -> &Vec<String> {
        &self.modes
    }
}

impl HasPayload for Ready {}

#[derive(Debug, Deserialize)]
pub struct SessionDescription {
    mode: String,
    secret_key: Vec<u8>,
}

impl SessionDescription {
    pub fn full_secret_key(self) -> Vec<u8> {
        self.secret_key
    }

    pub const fn mode(&self) -> &String {
        &self.mode
    }
}

impl HasPayload for SessionDescription {}

#[derive(Debug, Serialize)]
pub struct SelectInner {
    address: String,
    port: u16,
    mode: String,
}

#[derive(Debug, Serialize)]
pub struct Select {
    protocol: String,
    data: SelectInner,
}

impl Select {
    pub fn new(address: String, port: u16, mode: String) -> Self {
        Self {
            protocol: String::from("udp"),
            data: SelectInner {
                address,
                port,
                mode,
            },
        }
    }
}

impl HasPayload for Select {}

#[derive(Debug, Serialize)]
pub struct Speaking {
    speaking: u8,
    delay: usize,
    ssrc: u32,
}

impl Speaking {
    pub const fn new(speaking: u8, delay: usize, ssrc: u32) -> Self {
        Self {
            speaking,
            delay,
            ssrc,
        }
    }
}

impl HasPayload for Speaking {}
