use nom::{
    bytes::complete::{tag, take},
    character::complete::{anychar, char},
    error::{Error, ErrorKind},
    number::complete::{le_u32, le_u64},
    IResult,
};

bitflags! {
    pub struct PageType: u8 {
        const CONTINUATION = 0x01;
        const BOS = 0x02;
        const EOS = 0x04;
    }
}

#[derive(Debug)]
pub struct Page<'a> {
    page_type: PageType,
    granule: u64,
    bitstream_number: u32,
    seq_num: u32,
    checksum: u32,
    segments: Vec<&'a [u8]>,
}

impl<'a> Page<'a> {
    pub const fn page_type(&self) -> PageType {
        self.page_type
    }

    pub const fn segments(&self) -> &Vec<&'a [u8]> {
        &self.segments
    }

    pub fn granule(&self) -> u64 {
        self.granule
    }
}

/// https://datatracker.ietf.org/doc/html/rfc3533#section-6
pub fn parse_page(input: &[u8]) -> IResult<&[u8], Page<'_>> {
    // magic number.
    let (input, _) = tag("OggS")(input)?;
    // still 0ver :(
    let (input, _) = char(0 as char)(input)?;

    // page type
    let (input, page_type) = anychar(input)?;
    let page_type = PageType::from_bits(page_type as u8)
        .ok_or_else(|| nom::Err::Failure(Error::new(input, ErrorKind::ParseTo)))?;

    // granule position (btw yes this contains pcm samples after every packet on this page finish
    //    for opus at least).
    // if this is -1 two's complement (u64::MAX), then there were no packets ending on this page.
    let (input, granule) = le_u64(input)?;

    // logical bitstream serial number, this doesn't really matter
    let (input, bitstream_number) = le_u32(input)?;

    // sequence number of packet, used for detecting dropped packets on a logical bitstream.
    let (input, seq_num) = le_u32(input)?;

    // checksum, imho not worth checking in the slightest :D
    // plus it's not your standard crc32 iirc, you'll have to do some work to check it.
    let (input, checksum) = le_u32(input)?;

    // get the lengths (lacing values?) of segments
    let (input, segment_table_length) = anychar(input)?;
    let segment_table_length = segment_table_length as u8;
    let (mut input, lacing_values) = take(segment_table_length)(input)?;

    // segments in this page
    let mut segments: Vec<&[u8]> = Vec::new();
    for lacing_value in lacing_values {
        let (new_input, segment) = take(*lacing_value)(input)?;
        input = new_input;
        segments.push(segment);
    }

    Ok((
        input,
        Page {
            page_type,
            granule,
            bitstream_number,
            seq_num,
            checksum,
            segments,
        },
    ))
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        let contents = std::fs::read("example.opus").expect("something bad");
        let (rest, _) = super::parse_page(contents.leak()).expect("parsing failed");
        let (rest, _) = super::parse_page(rest).expect("parsing failed");
        let (rest, page) = super::parse_page(rest).expect("parsing failed");

        dbg!(page);
    }
}
