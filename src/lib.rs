#[macro_use]
extern crate bitflags;

mod ogg;
mod payloads;

use byteorder::{BigEndian, ByteOrder};
use bytes::{BufMut, BytesMut};
use futures::prelude::stream::*;
use futures::{SinkExt, StreamExt};
use nom::AsBytes;
use std::convert::TryInto;
use std::iter::FromIterator;
use std::net::SocketAddrV4;
use std::sync::Arc;
use tokio::net::UdpSocket;
use tokio::sync::oneshot;
use tokio::sync::Mutex;
use tokio::time::sleep;
use tokio_tungstenite::tungstenite::Message;
use tokio_tungstenite::{MaybeTlsStream, WebSocketStream};
use twilight_model::gateway::payload;
use xsalsa20poly1305::aead::generic_array::GenericArray;
use xsalsa20poly1305::aead::{Aead, NewAead};
use xsalsa20poly1305::XSalsa20Poly1305;

type WSWrite = SplitSink<WebSocketStream<MaybeTlsStream<tokio::net::TcpStream>>, Message>;

// what is this conspiracy?
// why are all packets 20ms long?
// nobody knows.
//
// except for discord, who requires 20ms packets, supposedly.
const SAMPLES_PER_PACKET: u32 = 960;

pub async fn connect_to_voice_gateway(
    state_evt: Box<payload::VoiceStateUpdate>,
    server_evt: payload::VoiceServerUpdate,
) {
    let session_id = (*state_evt).0.session_id;
    let endpoint = String::from("wss://")
        + server_evt.endpoint.expect("no endpoint recv-ed").as_str()
        + "?v=4";
    let token = server_evt.token;
    let guild_id = server_evt.guild_id.expect("dm call?");
    let user_id = (*state_evt).0.user_id;

    let mut url = url::Url::parse(endpoint.as_str()).expect("bad endpoint");

    url.set_port(Some(443)).unwrap();

    let config = tokio_tungstenite::tungstenite::protocol::WebSocketConfig {
        accept_unmasked_frames: false,
        max_frame_size: None,
        max_message_size: None,
        max_send_queue: None,
    };

    // tokio join handles
    let mut heartbeat_handle = None;
    let mut udp_handle = None;

    let (ws, _) = tokio_tungstenite::connect_async_with_config(url, Some(config))
        .await
        .expect("unable to connect to endpoint");
    let (write, mut read) = ws.split();

    let write = Arc::new(Mutex::new(write));

    let mut session_description_handler = None;

    let mut ssrc = None;

    write
        .lock()
        .await
        .send(
            serde_json::to_string(&payloads::Payload::new(
                0,
                payloads::Identify::new(
                    guild_id.to_string(),
                    user_id.to_string(),
                    session_id,
                    token,
                ),
            ))
            .unwrap()
            .into(),
        )
        .await
        .expect("ws closed");

    while let Some(Ok(msg)) = read.next().await {
        match msg {
            Message::Text(payload) => {
                #[derive(serde::Deserialize)]
                struct Payload {
                    op: u8,
                }

                println!("msg: {:?}", payload);

                let payload_op: Payload = serde_json::from_str(payload.as_str()).unwrap();

                match payload_op.op {
                    8 => {
                        let payload: payloads::Payload<payloads::Hello> =
                            serde_json::from_str(payload.as_str()).unwrap();
                        let hello = payload.d();
                        let heartbeat_interval = hello.heartbeat_interval();

                        if heartbeat_handle.is_none() {
                            heartbeat_handle =
                                Some(tokio::spawn(heartbeater(write.clone(), heartbeat_interval)));
                        }
                    }
                    2 => {
                        let payload: payloads::Payload<payloads::Ready> =
                            serde_json::from_str(payload.as_str()).unwrap();
                        let ready = payload.full_d();

                        ssrc = Some(ready.ssrc());

                        let (tx, rx) = oneshot::channel();
                        let (tx2, rx2) = oneshot::channel();

                        session_description_handler = Some(tx);

                        udp_handle = Some(tokio::spawn(connect_to_voice_udp(ready, rx, tx2)));

                        let val = rx2.await.expect("discord never responded to ip discovery");
                        write
                            .lock()
                            .await
                            .send(
                                serde_json::to_string(&payloads::Payload::new(
                                    1,
                                    payloads::Select::new(
                                        val.address,
                                        val.port,
                                        String::from("xsalsa20_poly1305"),
                                    ),
                                ))
                                .unwrap()
                                .into(),
                            )
                            .await
                            .expect("voice gateway closed");
                    }
                    6 => {
                        // pong, should this check nonce?
                    }
                    4 => {
                        let payload: payloads::Payload<payloads::SessionDescription> =
                            serde_json::from_str(payload.as_str()).unwrap();
                        let description = payload.full_d();

                        if description.mode() != "xsalsa20_poly1305" {
                            todo!("other encryption modes");
                        }

                        write
                            .lock()
                            .await
                            .send(
                                serde_json::to_string(&payloads::Payload::new(
                                    5,
                                    payloads::Speaking::new(1u8, 0, ssrc.unwrap()),
                                ))
                                .unwrap()
                                .into(),
                            )
                            .await
                            .expect("voice gateway closed");

                        session_description_handler
                            .take()
                            .unwrap()
                            .send(description.full_secret_key())
                            .unwrap();
                    }
                    num => {
                        eprintln!("Missing match pattern: {:?}", num);
                    }
                }
            }
            Message::Close(frame) => {
                eprintln!("closing with {:?}", frame);
                if let Some(handle) = heartbeat_handle.take() {
                    handle.abort();
                }
                if let Some(handle) = udp_handle.take() {
                    handle.abort();
                }
                return;
            }
            // binary should never happen
            // and other two are dealt with for us? maybe.
            Message::Binary(_) | Message::Ping(_) | Message::Pong(_) => {}
        }
    }
}

async fn heartbeater(ws: Arc<Mutex<WSWrite>>, interval: f64) {
    loop {
        tokio::time::sleep(tokio::time::Duration::from_secs_f64(interval / 1000.0)).await;
        let payload = payloads::Payload::new(3, rand::random::<u32>());
        ws.lock()
            .await
            .send(serde_json::to_string(&payload).unwrap().into())
            .await
            .expect("ws closed");
    }
}

#[derive(Debug)]
struct AddrInfo {
    port: u16,
    address: String,
}

async fn connect_to_voice_udp(
    ready: payloads::Ready,
    session_description: oneshot::Receiver<Vec<u8>>,
    address_info: oneshot::Sender<AddrInfo>,
) -> std::io::Result<()> {
    if !ready.modes().contains(&String::from("xsalsa20_poly1305")) {
        todo!("other encryption modes");
    }

    let socket = UdpSocket::bind("0.0.0.0:0")
        .await
        .expect("was not able to bind");

    socket
        .connect(SocketAddrV4::new(ready.ip().parse().unwrap(), ready.port()))
        .await
        .expect("cannot connect to discord");

    let mut packet = ip_discovery_packet(ready.ssrc());
    let _send_len = socket.send(&packet).await?;
    let len = socket.recv(&mut packet).await?;

    if len != 74 {
        panic!("discord sent bad ip discovery packet in response");
    }

    let address: [u8; 72 - 8] = TryInto::try_into(&packet[8..72]).unwrap();
    let zero_addr = address
        .iter()
        .position(|n| n == &0)
        .expect("discord sent bad ip discovery packet");
    let address = String::from_utf8(address[..zero_addr].to_vec())
        .expect("discord sent bad ip discovery packet");
    let port = BigEndian::read_u16(&packet[72..74]);

    address_info
        .send(AddrInfo {
            port,
            address: address.clone(),
        })
        .unwrap();

    let secret_key = session_description
        .await
        .expect("discord never sent secret key");

    let cipher = XSalsa20Poly1305::new(GenericArray::from_slice(secret_key.as_slice()));

    let contents = tokio::fs::read("example.opus")
        .await
        .expect("no example file found (example.opus)");
    let (contents, _) = ogg::parse_page(contents.as_bytes()).unwrap();
    let (mut contents, mut page) = ogg::parse_page(contents).unwrap();
    let mut continuation: Vec<u8> = Vec::new();
    let mut sequence: u16 = rand::random();
    let mut timestamp: u32 = rand::random();
    let mut prev_granule = 0u64;

    while page.page_type() != ogg::PageType::EOS {
        let (new_contents, new_page) = ogg::parse_page(contents).unwrap();
        contents = new_contents;
        page = new_page;

        // let (new_contents, new_continuation) = page.packets(continuation);
        // continuation = new_continuation;

        let mut body = BytesMut::from_iter(continuation);
        let start = std::time::Instant::now();
        for segment in page.segments() {
            let len = segment.len();

            body.extend(*segment);

            if len != 255 {
                send_data(
                    &socket,
                    body.as_bytes(),
                    sequence,
                    timestamp,
                    ready.ssrc(),
                    &cipher,
                )
                .await;

                timestamp += SAMPLES_PER_PACKET;
                sequence += 1;
                body.clear();
            }
        }

        continuation = body.to_vec();

        sequence += 1;

        let time_change: f64 = (page.granule() - prev_granule) as f64;
        prev_granule = page.granule();

        sleep(
            tokio::time::Duration::from_secs_f64(time_change / 48000.0)
                - (std::time::Instant::now() - start),
        )
        .await;
    }

    Ok(())
}

fn ip_discovery_packet(ssrc: u32) -> [u8; 74] {
    let mut packet = [0u8; 74];
    packet[0..2].swap_with_slice(&mut 1u16.to_be_bytes());
    packet[2..4].swap_with_slice(&mut 70u16.to_be_bytes());
    packet[4..8].swap_with_slice(&mut ssrc.to_be_bytes());
    packet
}

async fn send_data(
    sock: &UdpSocket,
    data: &[u8],
    sequence: u16,
    timestamp: u32,
    ssrc: u32,
    crypto: &XSalsa20Poly1305,
) {
    let mut header = [0u8; 12];
    header[0] = 0x80;
    header[1] = 0x78;
    header[2..4].swap_with_slice(&mut sequence.to_be_bytes());
    header[4..8].swap_with_slice(&mut timestamp.to_be_bytes());
    header[8..12].swap_with_slice(&mut ssrc.to_be_bytes());

    let mut packet = BytesMut::from(&header[..]);

    let mut nonce = [0u8; 24];
    nonce[..12].swap_with_slice(&mut header);

    packet.put(&*encrypt(data, nonce, crypto).leak());

    sock.send(packet.as_bytes())
        .await
        .expect("closed udp socket");
}

fn encrypt(data: &[u8], nonce: [u8; 24], crypto: &XSalsa20Poly1305) -> Vec<u8> {
    let nonce = GenericArray::from(nonce);
    crypto.encrypt(&nonce, data).expect("was unable to encrypt")
}
